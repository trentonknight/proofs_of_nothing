# COQ setup on Arch Linux
ocaml missing libs for proper use of [opam](https://opam.ocaml.org/doc/Install.html)
```bash
sudo pacman -Sy ocaml-compiler-libs
```
[ocaml](https://opam.ocaml.org/doc/Install.html) install
```bash
sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
opam init
```
## Font for mathematics, greek and symbols
[https://wiki.archlinux.org/index.php/Fonts](https://wiki.archlinux.org/index.php/Fonts)
```bash
sudo pacman -Sy ttf-dejavu bdf-unifont terminus-font ttf-linux-libertine noto-fonts noto-fonts-extra ttf-fira-sans ttf-fira-mono ttf-ubuntu-font-family ttf-croscore ttf-roboto ttf-inconsolata ttf-opensans noto-fonts-emoji texlive-core texlive-fontsextra otf-latin-modern otf-latinmodern-math otf-cascadia-code ttf-cascadia-code woff2-cascadia-code ttf-sarasa-gothic ttf-fira-code ttf-nerd-fonts-symbols
```
Add AUR package
```bash
git clone https://aur.archlinux.org/font-symbola.git
```
Gather latest information on fonts inside .emacs.d libraries.
```bash
emacs .emacs.d/layers/+fonts/unicode-fonts/README.org

```
## Installing Proof Asistant support on standard EMACS 
* [emacs](https://www.gnu.org/software/emacs/refcards/pdf/survival.pdf)
* [Proof General](https://proofgeneral.github.io/)
```bash
pacman -Sy emacs
```
Verify melpa is within the .emacs file
```bash
(require 'package)
;; (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") ; see remark below
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
```
## Melpa
Review Melpa for new packages available to emacs or spacemacs.
* [https://melpa.org/#/](https://melpa.org/#/)
* Install [Proof General](https://proofgeneral.github.io/) and [Company-coq](https://github.com/cpitclaudel/company-coq)
```bash
M-x package-refresh-contents RET
M-x package-install RET proof-general
M-x package-install RET company-coq RET
M-x package-install RET gruvbox-theme
```
Add Company-coq to .emacs
```bash
;; Load company-coq when opening Coq files
(add-hook 'coq-mode-hook #'company-coq-mode)
```
### View Company Coq tutorial for Logix and Math Symbol use
```bash
M-x company-coq-tutorial
```
## Manually using unicode characters
```bash
Ctrl+X 8
```
Select unicode number from here
* [https://en.wikipedia.org/wiki/Mathematical_operators_and_symbols_in_Unicode](https://en.wikipedia.org/wiki/Mathematical_operators_and_symbols_in_Unicode)
* [http://ergoemacs.org/emacs/emacs_n_unicode.html](http://ergoemacs.org/emacs/emacs_n_unicode.html)

## Update [emacs](https://www.gnu.org/software/emacs/refcards/pdf/survival.pdf) theme before going blind editing .emacs once again
```bash
(use-package gruvbox-theme
  :init (progn (load-theme 'gruvbox-dark-hard t t)
               (enable-theme 'gruvbox-dark-hard))
  :defer t
  :ensure t)
```
## GTK theme for emacs
Command to use custom GTK theme with emacs for reducing eye strain. 
```bash
GTK_THEME=Adwaita:dark emacs foofile.v
```
## Powerline for emacs with airline-themes
Look at varion theme names here:
[https://github.com/vim-airline/vim-airline/wiki/Screenshots](https://github.com/vim-airline/vim-airline/wiki/Screenshots)
```bash
(require 'airline-themes)
;;(load-theme 'airline-term t)
;;(load-theme 'airline-wombat t)
;;(load-theme 'airline-molokai t)
(load-theme 'airline-angr t)
```


# Enabling Proof Assistant packages in SpaceMacs 
## Enable spacemacs in .emacs first
* [https://www.spacemacs.org/](https://www.spacemacs.org/)
```bash
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```
### To make configuration changes review layers here:
* [https://www.spacemacs.org/layers/LAYERS.html](https://www.spacemacs.org/layers/LAYERS.html)

### VIM users cheat sheet for Spacemacs
* [https://www.spacemacs.org/doc/VIMUSERS.html](https://www.spacemacs.org/doc/VIMUSERS.html)
## .spacemacs dotspace-configuration-layers
Use spacebar + f, e, d to begin configuration in .spacemacs file. append the following under dotspacemacs-configuration-layers:
* coq
* unicode-fonts
### .spacemacs defun dotspacemacs/user-config
(global-prettify-symbols-mode 1)
(proof-unicode-tokens-enable 1)
#### Other proof assistants 
Use M+x aka space bar twice in spacemacs to search other layer features and type: proofgeneral. You should see options for coq, easycrypt, phox, pgshell, pgocaml and pghaskell. Type in unicode to view unicode layered features and commands.
#### Type the following in to verify symbols are working in a coq file ending in .v: 
```bash
forall A in B (A < C and not D)
```

* [https://github.com/cpitclaudel/company-coq/blob/master/refman/tutorial.v](https://github.com/cpitclaudel/company-coq/blob/master/refman/tutorial.v)
```bash
curl -O https://raw.githubusercontent.com/cpitclaudel/company-coq/master/refman/tutorial.v
```
# Tutorials and References
* [Emacs Survival Card](https://www.gnu.org/software/emacs/refcards/pdf/survival.pdf)
* [Open Logic Project](https://github.com/OpenLogicProject)
* [Proof General](https://proofgeneral.github.io/)
* [Company Coq](https://github.com/cpitclaudel/company-coq)
* [BASICS Functional Programming in COQ](https://www.cs.cornell.edu/courses/cs4160/2020sp/sf/lf/terse/Basics.html)
